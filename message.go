package delcom

// Go library for controlling Delcom USB HID vizualizers

import ()

// Major commands
const (
	ReadMajor  = 100 // Read status registers
	Write8bit  = 101 // Write commands to device
	Write16bit = 102 // Write buzzer commands
)

// Minor command codes, write
const (
	CmdPort0        = 1  // Set port0 pins
	CmdPort1        = 2  // Set port1 pins
	CmdBothPorts    = 10 // Set both port0 and 1 pins
	CmdSetReset0    = 11 // Set and reset pins on port 0
	CmdSetReset1    = 12 // Set and reset pins on port 1
	CmdClkPrescaler = 19 // Set clock prescaler for all leds
	CmdClkSetReset  = 20 // Set and reset flashing on pins
	CmdSetDuty0     = 21 // Set duty cycle for port 1 pin 0
	CmdSetDuty1     = 22 // Set duty cycle for port 1 pin 1
	CmdSetDuty2     = 23 // Set duty cycle for port 1 pin 2
	CmdSync         = 25 // Sync clock generators
	CmdDelay0       = 26 // Phase delay for port 1 pin 0
	CmdDelay1       = 27 // Phase delay for port 1 pin 1
	CmdDelay2       = 28 // Phase delay for port 1 pin 2
	CmdDelay3       = 29 // Phase delay for port 1 pin 3 (buzzer)
	CmdPWM          = 34 // Set PWM levels for port 1 pins
	CmdEvCounter    = 38 // Enable or disable the event counter
	CmdBuzzer       = 70 // Buzzer control
	CmdAutoClear    = 72 // Auto clear and auto confirm control
)

// Minor commands, read
const (
	CmdReadEvents = 8   // Read the event counter value
	CmdReadFW     = 10  // Read the firmware version
	CmdReadPorts  = 100 // Read port status
)

// Pins for set/reset messages
const (
	Pin0 = 1
	Pin1 = 2
	Pin2 = 4
	Pin3 = 8
)

// Message format for USB HID feature packets (write)
type DelComMsg struct {
	MajorCmd byte    // Major command code
	MinorCmd byte    // Minor command code
	DataLSB  byte    // LSB of payload data
	DataMSB  byte    // MSB of payload data
	Extra    [4]byte // Possible extra data
}

func InitMsg(major byte, minor byte) *DelComMsg {
	msg := DelComMsg{
		MajorCmd: major,
		MinorCmd: minor,
		DataLSB:  0,
		DataMSB:  0,
		Extra:    [4]byte{0, 0, 0, 0},
	}
	return &msg
}

func (msg *DelComMsg) Marshal() []byte {
	bytes := make([]byte, 8)
	bytes[0] = msg.MajorCmd
	bytes[1] = msg.MinorCmd
	bytes[2] = msg.DataLSB
	bytes[3] = msg.DataMSB
	bytes[4] = msg.Extra[0]
	bytes[5] = msg.Extra[1]
	bytes[6] = msg.Extra[2]
	bytes[7] = msg.Extra[3]

	return bytes
}
