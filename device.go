package delcom

import (
	"errors"
	"github.com/karalabe/hid"
)

const delcomID = 0x0fc5

// Device represents a single delcom device
type Device struct {
	hidDev *hid.Device
}

// OpenDevices opens all attached delcom devices matching a model number
func OpenDevices(productID uint16) ([]Device, error) {
	devs := hid.Enumerate(delcomID, productID)
	ret := make([]Device, len(devs))

	for i, dev := range devs {
		d, err := dev.Open()
		if err != nil {
			return nil, err
		}
		ret[i] = Device{
			hidDev: d,
		}
		err = ret[i].Init()
		if err != nil {
			return nil, err
		}
	}
	return ret, nil
}

// Init loads initial defaults in
func (dev *Device) Init() error {
	// Set the PWM brighness
	for i := byte(0); i < 3; i++ {
		err := dev.SetPWM(i, 50)
		if err != nil {
			return err
		}
	}
	// Disable all clock generators
	err := dev.EnableFlash(0, 0xff)
	if err != nil {
		return err
	}
	// Turn off all leds
	err = dev.SetPort(1, 0xff)
	if err != nil {
		return err
	}
	return nil
}

func (dev *Device) Close() {
	dev.SetPort(1, 0xff)
	dev.EnableFlash(0, 0xff)
	dev.hidDev.Close()
}

// SetPWM sets the PWM duty cycle for a pin. Pin values 0-3, duty values 0-100
func (dev *Device) SetPWM(pin byte, duty byte) error {
	if pin > 3 {
		return errors.New("Illegal pin value")
	}
	if duty > 100 {
		return errors.New("Illegal duty cycle value")
	}

	msg := InitMsg(Write8bit, CmdPWM)
	msg.DataLSB = pin
	msg.DataMSB = duty
	return dev.sendMsg(msg)
}

// SetFlashDuty sets the frequency and duty cycle for a pin. Pin values 0-2.
func (dev *Device) SetFlashDuty(pin byte, on byte, off byte) error {
	if pin > 2 {
		return errors.New("Illegal port value")
	}
	msg := InitMsg(Write8bit, CmdSetDuty0+pin)
	msg.DataLSB = off
	msg.DataMSB = on
	return dev.sendMsg(msg)
}

// SetClockScaler sets the global clock prescaler
func (dev *Device) SetClockScaler(scaler byte) error {
	msg := InitMsg(Write8bit, CmdClkPrescaler)
	msg.DataLSB = scaler
	return dev.sendMsg(msg)
}

// EnableFlash enables and disables flashing on pins
func (dev *Device) EnableFlash(enable byte, disable byte) error {
	msg := InitMsg(Write8bit, CmdClkSetReset)
	msg.DataLSB = disable
	msg.DataMSB = enable
	return dev.sendMsg(msg)
}

// SetPort sets port pins
func (dev *Device) SetPort(port byte, value byte) error {
	if port > 1 {
		return errors.New("Illegal port number")
	}
	msg := InitMsg(Write8bit, CmdPort0+port)
	msg.DataLSB = value
	return dev.sendMsg(msg)
}

// SetBoth sets both ports 0 and 1
func (dev *Device) SetBoth(port0 byte, port1 byte) error {
	msg := InitMsg(Write8bit, CmdBothPorts)
	msg.DataLSB = port0
	msg.DataMSB = port1
	return dev.sendMsg(msg)
}

// SetResetPins sets and resets invidial pins on a port.
func (dev *Device) SetResetPins(port byte, enable byte, disable byte) error {
	if port > 1 {
		return errors.New("Illegal port number")
	}
	msg := InitMsg(Write8bit, CmdSetReset0+port)
	msg.DataLSB = disable
	msg.DataMSB = enable
	return dev.sendMsg(msg)
}

// SetInitDelay sets the initial phase delay for a pin.
func (dev *Device) SetInitDelay(pin byte, delay byte) error {
	if pin > 3 {
		return errors.New("Illegal pin number")
	}
	msg := InitMsg(Write8bit, CmdDelay0+pin)
	msg.DataLSB = delay
	return dev.sendMsg(msg)
}

// FlashSync syncs the flashing clocks on pins.
func (dev *Device) FlashSync(pins byte) error {
	msg := InitMsg(Write8bit, CmdSync)
	msg.DataLSB = pins
	return dev.sendMsg(msg)
}

// SetEventCounter enables or disables the event counter
func (dev *Device) SetEventCounter(state byte) error {
	msg := InitMsg(Write8bit, CmdEvCounter)
	msg.DataLSB = state
	return dev.sendMsg(msg)
}

// Buzzer controls the buzzer.
func (dev *Device) Buzzer(on byte, freq byte, repeat, onTime byte, offTime byte) error {
	msg := make([]byte, 16)
	msg[0] = Write16bit
	msg[1] = CmdBuzzer
	msg[2] = on
	msg[3] = freq
	msg[8] = repeat
	msg[9] = onTime
	msg[10] = offTime
	_, err := dev.hidDev.SendFeatureReport(msg)
	return err
}

func (dev *Device) sendMsg(msg *DelComMsg) error {
	_, err := dev.hidDev.SendFeatureReport(msg.Marshal())
	return err
}
