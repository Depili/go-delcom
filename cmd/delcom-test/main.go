package main

import (
	"gitlab.com/Depili/go-delcom"
	"log"
	"time"
)

func main() {
	devs, _ := delcom.OpenDevices(0xb080)
	for _, dev := range devs {
		dev.SetPort(1, 0xff)

		for i := byte(0); i < 3; i++ {
			dev.SetPWM(i, 50)
		}
		dev.EnableFlash(0, delcom.Pin0+delcom.Pin1+delcom.Pin2)

		log.Printf("Green")
		dev.SetPort(1, 0xfe)
		time.Sleep(time.Second)
		log.Printf("Red")
		dev.SetPort(1, 0xfd)
		time.Sleep(time.Second)
		log.Printf("Yellow / blue")
		dev.SetPort(1, 0xfb)
		time.Sleep(time.Second)
		log.Printf("Turning all off")
		dev.SetPort(1, 0xff)
		time.Sleep(time.Second)

		log.Printf("Toggling pins")

		log.Printf("Green")
		dev.SetResetPins(1, 0, delcom.Pin0)
		time.Sleep(time.Second)
		log.Printf("Red")
		dev.SetResetPins(1, delcom.Pin0, delcom.Pin1)
		time.Sleep(time.Second)
		log.Printf("Yellow / blue")
		dev.SetResetPins(1, delcom.Pin1, delcom.Pin2)
		time.Sleep(time.Second)

		dev.SetPort(1, 0xff)

		dev.SetFlashDuty(0, 25, 75)
		dev.SetFlashDuty(1, 75, 25)
		dev.SetFlashDuty(2, 50, 50)

		log.Printf("Flashing green")
		dev.EnableFlash(delcom.Pin0, 0)
		time.Sleep(time.Second * 3)

		log.Printf("Flashing red")
		dev.EnableFlash(delcom.Pin1, delcom.Pin0)
		time.Sleep(time.Second * 3)

		log.Printf("Flashing yellow / blue")
		dev.EnableFlash(delcom.Pin2, delcom.Pin1)
		time.Sleep(time.Second * 3)

		dev.EnableFlash(0, delcom.Pin0+delcom.Pin1+delcom.Pin2)
	}
}
